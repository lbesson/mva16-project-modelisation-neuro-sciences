## Code review and evaluation
> Generated with [pylint](https://www.pylint.org/) command line program.

### For the programs:

- [for `kmeans.py`](../kmeans.py): review on [kmeans.pylint.txt](./kmeans.pylint.txt);

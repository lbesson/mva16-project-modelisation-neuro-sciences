## Research report
- LaTeX source for my [research report](./MVA__Modelisation_Neuro_Sciences__project__Lilian_Besson__2015-16__Report.en.tex),
- See [here for the PDF report](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/downloads/MVA__Modelisation_Neuro_Sciences__project__Lilian_Besson__2015-16__Report.en.pdf) (not yet finished),
- It should be easy to compile the report again, by using [this Makefile](./Makefile) (``make pdf`` would do the job),
- The bibliography is from [this folder](../biblio/) (and uses [this style](./naereen.bst)),
- The figures are from [this folder](../fig/) (generated by [these programs](../src/)).

#### More ?
- [The slides](../slides/) of my 15-minute talk, they should provide a clean and short document to start with,
- See [the code](../src/) for details about the numerical experiments and implementation (in [Python](https://www.python.org/)).

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)

# *Outline* for the slides
# *« Self-Organizing Maps (SOM) and Dynamic SOM, unsupervised clustering and models of cortical plasticity »*

> Along with references, images and links for [the slides](./).

----
----

## 0. Introduction
### 0.1. Who am I / What these slides are for
- [Lilian Besson](http://perso.crans.org/besson/);
- Slides for the [oral presentation](http://www.lps.ens.fr/~nadal/Cours/MVA/odp2016.html) for the [Modelisation in Neuro-Sciences](http://www.lps.ens.fr/%7Enadal/Cours/MVA/) course, Master MVA 2015-2016.

### 0.2. Presentation
#### 0.2.a. Goals of this talk
- 3 key words: Unsupervised learning, self-organizing maps (SOM) and Dynamic SOM.

#### 0.2.b. [Outline](./outline.md)
- Automatically generated in LaTeX, OK

----
----

## Part 1 : Supervised or Unsupervised learning
- What are the main difference between supervised or unsupervised learning?
- Why is unsupervised harder?
- FIXME

----

### 1.1. Different types of machine learning?
#### 1.1.a. Supervised learning: e.g. deep Learning
[Google Images](https://images.google.com/): real-world image retrieval works.

![Deep Learning success: Google Images](../fig/slides/deep_learning_google_images.png "Deep Learning success: Google Images")

#### 1.1.b. Reinforcement learning: e.g. Alpha Go
[Alpha Go (Google DeepMind)](https://www.deepmind.com/alpha-go.html)

![Reinforcement Learning success: Google DeepMind AlphaGo](../fig/slides/reinforcement_learning_google_deep_mind.png "Reinforcement Learning success: Google DeepMind AlphaGo")

#### 1.1.c. But the Graal is still the *unsupervised world*
[Yann LeCun quoting Richard Sutton](https://www.facebook.com/yann.lecun/posts/10153442884182143):

> « Richard S. Sutton the father of reinforcement learning, comments on the AlphaGo victory :
> "AlphaGo is missing one key thing: the ability to learn how the world works."
> [Yann LeCun] totally agrees. Richard has long advocated that the ability to predict is an essential component of intelligence.
> **Predictive (unsupervised) learning is one of the things some of us see as the next obstacle to better AI**. »

![Richard Sutton says unsupervised learning is still not attained](../fig/slides/unsupervised_learning__yann_lecun_richard_sutton.png "Richard Sutton says unsupervised learning is still not attained")

----

### 1.2. An unsupervised problem: vector quantization / clustering
#### 1.2.a. Vector quantization
- Explain what has to be done

![For 2D points, examples of a bad quantization and a good quantization](../fig/slides/quantization_for_2D_points.png "For 2D points, examples of a bad quantization and a good quantization")

#### 1.2.b. Notations
- Mathematical notations (from N. Rougier's slide #3/17)
- Set Q, function f etc

#### 1.2.c. A classical problem
- Algorithms: K-Means, Elastic Net, (Growing/Dynamic) Neural Gas, Neural Field, (Dynamic) Self-Organizing Maps...
- Applications: compression of data / images, automatic classification, categorization.

----

### 1.3. A well-known clustering algorithm: [K-Means](http://scikit-learn.org/dev/modules/clustering.html#k-means)
- The KMeans algorithm clusters data by trying to separate samples in *n* groups of equal variance, minimizing a criterion known as the *"inertia"* or within-cluster sum-of-squares.
- This algorithm requires K, the number of clusters, to be specified before-hand, as most unsupervised models.
- It scales *well* to large number of samples, and has been used across a large range of application areas in many different fields.

![K-Means clustering on the digits dataset (PCA-reduced data)](../fig/plot_kmeans_digits.png "K-Means clustering on the digits dataset (PCA-reduced data)")

#### 1.3.a. How does it work?
- The k-means algorithm divides a set of N samples X into K disjoint clusters C, each described by the mean ``\mu_j`` of the samples in the cluster.
- The means are commonly called the cluster "centroids": note that they are **not**, in general, points from X, although they live in the same space.
- The K-means algorithm aims to choose centroids that minimize the inertia, or within-cluster sum of squared criterion:

```latex
\sum_{i=0}^{n}\min_{\mu_j \in C}(||x_j - \mu_i||^2)
```

#### 1.3.b. Does it converge? Yes
- K-means is equivalent to the [Expectation-Maximization algorithm](http://scikit-learn.org/dev/modules/mixture.html#expectation-maximization) with a small, all-equal, diagonal covariance matrix;
- The [Expectation-Maximization algorithm](https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm) converges, as a distortion minimization algorithm.
- ... But it can fall down to *local* minima: that's why a *dynamic* unsupervised learning algorithm can be useful!

#### 1.3.c. Implementation and example
- Available in scikit-learn: [``sklearn.clustering.KMeans``](http://scikit-learn.org/dev/modules/generated/sklearn.clustering.KMeans.html),
- Also reimplemented my self, see [``kmeans.py``](../src/kmeans.py) for example (from homework #1 of the [PGM course](http://lbesson.bitbucket.org/pgm2016)).

#### 1.3.d. Application: color quantization
##### First example: Voronoï diagram in the color space
- For a picture with only two color channels (green/red): ![Rosa gold glow, from Wikipedia](../fig/color-quantization/Rosa_Gold_Glow_2_small_noblue.png "Rosa gold glow, from Wikipedia")

- In the color space, all the colors on this picture can be clustered into only 16 colors with [Voronoï diagrams](https://en.wikipedia.org/wiki/Voronoi_diagram)! ![Rosa gold glow, in the green/red color space, from Wikipedia](../fig/color-quantization/Rosa_Gold_Glow_2_small_noblue_color_space.png "Rosa gold glow, in the green/red color space, from Wikipedia")

##### Real-world example
With [a HD photo from Iceland](../fig/Heamey.jpg), [example](../fig/color-quantization/) [of color quantization](http://scikit-learn.org/dev/auto_examples/cluster/plot_color_quantization.html):

- Original picture: ![HD picture of Heamey (in Iceland), 3648x2736 pixels, 75986 colors](../fig/Heamey.jpg "HD picture of Heamey (in Iceland), 3648x2736 pixels, 75986 colors")
- Compressed picture, 32 colors from a random codebook: ![HD picture of Heamey (in Iceland), 3648x2736 pixels, 32 colors with random codebook](../fig/color-quantization/Heamey__random_quantized__32_colors.jpg "HD picture of Heamey (in Iceland), 3648x2736 pixels, 32 colors with random codebook")
- Compressed picture, 32 colors from a K-Means codebook: ![HD picture of Heamey (in Iceland), 3648x2736 pixels, 32 colors with KMeans](../fig/color-quantization/Heamey__KMean_quantized__32_colors.jpg "HD picture of Heamey (in Iceland), 3648x2736 pixels, 32 colors with KMeans")

![XXX](../fig/XXX "XXX")

----

## Part 2 : 2 classical unsupervised model coming from neuro-sciences

### 2.1. Common notations
> §2. from N. Rougier's article

----

### 2.2. Neural Gas
> §2.2. from N. Rougier's article

FIXME

- Formulation, equations
- Not dynamic: learning rate and width of neighborhood are decreasing with time, eventually getting to 0.

----

### 2.3. Self-Organizing Maps (Kohonen's maps)
> §2.1. from N. Rougier's article

#### 2.3.a. Kohonen's observation:
The visual cortex is spatially organized

![Retinotropic organization](../fig/slides/retinotopic_organization.png "Retinotropic organization")

#### 2.3.b. Mathematical formulation
- Not dynamic: learning rate and width of neighborhood are decreasing with time, eventually getting to 0.

FIXME

----

### 2.4. Application: color quantization again
FIXME also do a color quantization with a SOM ?
TODO code and figure

----
----

## Part 3 : Dynamic Self-Organizing Maps (DSOM)
> The core of the talk

> §2.3. from N. Rougier's article

### 3.1. What need for a *dynamic* extension of the SOM model?
FIXME

> §3. from N. Rougier's article

----

### 3.2. Mathematical formulation of the model
> §3.1. from N. Rougier's article

- Dynamic neighborhood

FIXME

----

### 3.3. Implementation
FIXME
TODO code and figure

----

### 3.4. Possible applications
FIXME

- Another clustering algorithm: same application than KMeans
- TODO code and figure
- Non stationary distribution (§4.1. + Fig.6 from N. Rougier's article)

![4 videos of dynamic topologies obtained by a DSOM](../fig/slides/4_videos_of_DSOM__dynamic_topologies.png "4 videos of dynamic topologies obtained by a DSOM")

----

### 3.5. A more serious experiment: cortical plasticity after a cardio-vascular incident
FIXME this experiment was done with a Dynamic Neural Field (DNF) not a DSOM

- TODO reproduce his experiment

----
----

## Part 4 : Possible improvements
> A few questions about DSOM that **we should try to answer**

### 4.1. Auto-tuning the elasticity parameter?
> §3.2. from N. Rougier's article

- 4.1.a. Present the problem
- 4.1.b. Try to answer

FIXME

----

### 4.2. Higher dimension topology?
> §4.2. from N. Rougier's article

- 4.2.a. Present the problem
- 4.2.b. Try to answer

FIXME

----

### 4.3. Topological rupture?
- 4.3.a. Present the problem
- 4.3.b. Try to answer

FIXME

----

### 4.4. Convergence and stability of the DSOM algorithm?
> §3.3. from N. Rougier's article

- 4.4.a. Present the problem
- 4.4.b. Try to answer

FIXME

----
----

## 5. Conclusion
> Finishing the talk

### 5.1. What did we see?
- Quick reminder of what was covered
- Reminder of the possible use of the model(s), its application and improvement regarding the first model
- Still some question to answer

FIXME

### 5.2. Thanks!
- For listening/reading and for the course
- Any feedback ? [Contact me](http://perso.crans.org/besson/contact/) or [fill a bug form](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/issues/new)

**End**

----

## Appendix
> Extra ressources

### A. Partial [list of references](../biblio/)
> 3-4 slides

### B. [License](http://lbesson.mit-license.org/) and [link to the git repository](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/)

**Real end**

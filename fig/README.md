## Figures
- These figures have been generated with [Python](https://www.python.org/), with [these programs](../src/),
- Some figures are from [reference article](../biblio/), in which case they are © of their respective authors,
- The figures are used in [the report](../report/) and [the slides](../slides/).

----

#### More ?
- See [the report](../report/) for more information explanations about these figures.
- See [the code](../src/), [the figures](../fig/) or [the references](../biblio/) if needed.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
